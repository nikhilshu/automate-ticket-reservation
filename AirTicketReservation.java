/*
 
 Project	 : Automate Ticket Reservation
 Category    : Case Study Project 
 Name 		 : Nikhil Kumar Shukla
 Email		 : nikhil.shukla2@wipro.com
 Date	     : 16 Sep 2019
 IDE 		 : Eclipse
 
 */


public class AirTicketReservation extends Reservation {
	String flightName ; 
	int numberOfTickets;
	 String bookingClass;

	 AirTicketReservation(String category, String customerName, String flightName,int numberOfTickets, String bookingClass)
	 {
		 super(category, customerName);

		 this.flightName=flightName;
		 this.numberOfTickets=numberOfTickets;
		 this.bookingClass=bookingClass;
	 }
	 String getFlightName()
	 {
		 return flightName;
	 }
	 

	 int getNumberOfTickets()
	 {
		 return numberOfTickets;
	 }
	 
	 void calulateAmount()
	 {
	 	int amount=0;
	 	if(bookingClass.equalsIgnoreCase("business"))
	 	{
	 		amount=4500;	
	 	}
	 	else if(bookingClass.equalsIgnoreCase("economy"))
	 	{
	 		amount=3500;	
	 	}
	 	
	 	
	 	amount=numberOfTickets*amount;
	 	System.out.println(" Amount : "+amount);
	 	
	 }

}
