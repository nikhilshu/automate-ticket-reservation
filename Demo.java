/*
 
 Project	 : Automate Ticket Reservation
 Category    : Case Study Project 
 Name 		 : Nikhil Kumar Shukla
 Email		 : nikhil.shukla2@wipro.com
 Date	     : 16 Sep 2019
 IDE 		 : Eclipse
 
 */


public class Demo{

	public static void main(String args[])
	{
		RailwayReservation objRailReservation=new RailwayReservation("train","ashok","rajdhani",2,"AC2");
		AirTicketReservation objAirTicketReservation=new AirTicketReservation("flight","anjana","airIndia",3,"Economy");
		System.out.println("\n***************************** Railway Ticket Reservation Details *****************************");
		
		System.out.println(" Transaction Number : "
				+objRailReservation.getTransactionNumber()
				+"\n Ticket Category : "
				+objRailReservation.getCategory()
				+"\n Customer Name : "
				+objRailReservation.getCustomerName()
				+"\n Train Name : "
				+objRailReservation.getTrainName()
				+"\n Number of Tickets : "
				+objRailReservation.getNumberOfTickets()
			);
		
		objRailReservation.calulateAmount();
		
		System.out.println("\n***************************** Air Ticket Reservation Details *****************************");
		
		System.out.println(" Transaction Number : "
		+objAirTicketReservation.getTransactionNumber()
		+"\n Ticket Category : "
		+objAirTicketReservation.getCategory()
		+"\n Customer Name : "
		+objAirTicketReservation.getCustomerName()
		+"\n Train Name : "
		+objAirTicketReservation.getFlightName()
		+"\n Number of Tickets : "
		+objAirTicketReservation.getNumberOfTickets()
	);

		objAirTicketReservation.calulateAmount();

	}
}
