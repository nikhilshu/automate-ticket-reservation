/*
 
 Project	 : Automate Ticket Reservation
 Category    : Case Study Project 
 Name 		 : Nikhil Kumar Shukla
 Email		 : nikhil.shukla2@wipro.com
 Date	     : 16 Sep 2019
 IDE 		 : Eclipse
 
 */


public class Reservation {
	static int transactionNumber=1;
	String Category;
	String customerName;
	 Reservation(String Category,String customerName)
	{
		this.Category=Category;
		this.customerName=customerName;
	}
	int getTransactionNumber()
	{
		return transactionNumber++;
	}
	String getCategory()
	{
		return Category;
	}
	String getCustomerName()
	{
		return customerName;
	}
	boolean validateCategory() 
	{
		if (Category.equalsIgnoreCase("train") || Category.equalsIgnoreCase("flight"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
