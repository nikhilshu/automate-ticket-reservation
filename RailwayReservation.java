/*
 
 Project	 : Automate Ticket Reservation
 Category    : Case Study Project 
 Name 		 : Nikhil Kumar Shukla
 Email		 : nikhil.shukla2@wipro.com
 Date	     : 16 Sep 2019
 IDE 		 : Eclipse
 
 */


public class RailwayReservation extends Reservation{
String trainName;
int numberOfTickets;	
String bookingClass;
RailwayReservation(String category,String customerName,String trainName,int numberOfTickets,String bookingClass)
{
	 super(category, customerName);

	 this.trainName=trainName;
	 this.numberOfTickets=numberOfTickets;
	 this.bookingClass=bookingClass;
}
String getTrainName()
{
return trainName;	
}
int getNumberOfTickets(){
	return numberOfTickets;
}
void calulateAmount()
{
	int amount=0;
	if(bookingClass.equalsIgnoreCase("AC1"))
	{
		amount=1500;	
	}
	else if(bookingClass.equalsIgnoreCase("AC2"))
	{
		amount=1100;	
	}
	else if(bookingClass.equalsIgnoreCase("AC3"))
	{
		amount=700;	
	}
	
	amount=numberOfTickets*amount;
	System.out.println(" Amount : "+amount);
	
}

}
